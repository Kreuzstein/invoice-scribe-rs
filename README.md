# invoice-scribe-rs
Simple command line tool for generating invoices. Inspired by https://github.com/maaslalani/invoice. 

## File Structure
```js
├── assets
│   └── fonts
│       └── IBMPlexSerif-Regular.ttf << put your font here
├── Cargo.lock
├── Cargo.toml
├── invoice.pdf <<<<<<<<<<<<<<<<<<<<<<< this will be your output file 
├── LICENSE
├── logo.png <<<<<<<<<<<<<<<<<<<<<<<<<< optionally use your logo here 
├── src
│   ├── config.rs <<<<<<<<<<<<<<<<<<<<< set your assets paths and default values here
│   ├── helpers
│   │   ├── combine_vectors.rs <<<<<<<< util for arranging billing items 
│   │   ├── generate_id.rs <<<<<<<<<<<< basic ID generator 
│   │   └── hyphenate.rs <<<<<<<<<<<<<< util for making IDs more human readable 
│   ├── main.rs 
│   └── pdf.rs <<<<<<<<<<<<<<<<<<<<<<<< pdf creation and support fns are here
```
## Installation
Use `cargo install` to install the application. This command will build the binary and place it in the installation directory:

```sh
cargo install --path .
```

By default, Cargo will install the binary to `~/.cargo/bin/`. Make sure that this directory is in your `PATH` environment variable. If it's not, you can add it with the following command:

```sh
echo 'export PATH="$HOME/.cargo/bin:$PATH"' >> ~/.bashrc
```

If you are using a shell other than Bash, you may need to modify the appropriate profile file, such as `~/.zshrc` for Zsh.
## Running
Run the command with the following arguments, like so:
```sh
>invoice_scribe \
> --from 'Quorum Socks Emporium' \
> --to 'Big Sock-O Warehouse' \
> --item Socks \
> --qty 1 \
> --rate 25 \
> --item Tights \
> --qty 2 \
> --rate 15.75 \
> --note 'No Comment.' \
> --tax 15 \
> --discount 5 \
> --currency 'GBP'
```

For multiple items you can either add them consequentially, or as a comma-separated string, like so:
```sh
>invoice_scribe \
> --from 'Quorum Socks Emporium' \
> --to 'Big Sock-O Warehouse' \
> --item Pnaty,Stockings \
> --qty 1,2 \
> --rate 25,12.5 \
```

You can also use the following short argument codes:
```sh
>invoice_scribe \
> -f 'Quorum Socks Emporium' \
> -t 'Big Sock-O Warehouse' \
> -i 'Socks,Panties' \
> -q '1,3' \
> -r '25,120.25' \
> -n 'No Comment.' \
> -x 15 \
> -% 5
> -c 'GBP'
```

Instead of running through the PATH variables, you can run locally with cargo:
```sh
cargo run -- %% your args go here %%
```
For example:
```sh
cargo run -- --from 'Quorum Socks Emporium' --to 'Big Sock-O Warehouse' --item Socks --qty 1 --rate 25 --item Tights --qty 2 --rate 15.75 --note 'No Comment.' --tax 15 --discount 50 -c SAR
```
## Defaults
Not specifying any field will assume the default value from `config.rs`:
```rust
// Defaults
pub const FROM: &str = "My Company";
pub const TO: &str = "My Client";
pub const ITEM: &str = "My Service";
pub const QTY: &str = "1"; // Must be an integer
pub const RATE: &str = "0.";
pub const TAX: f32 = 0.; // Input as a percentage, e.g. 20 or 20.0 for 20%
pub const DISCOUNT: f32 = 0.; // Input as a percentage, e.g. 20 or 20.0 for 20%
pub const CURRENCY: &str = "£";
pub const NOTE: &str = "";
```

Don't forget to re-build and re-install the binary to apply your changes.  
```sh
cargo install --path .
```
## Assets
You may add a your logo and specify it in the `config.rs` as well. You can also add a different font. Not that logo is optional but at least one font file should be provided:
```rust
// Path to assets
pub const LOGO_PATH: &str = "logo.png";
pub const FONT_PATH: &str = "assets/fonts/IBMPlexSerif-Regular.ttf";
```

Don't forget to re-build and re-install the binary to apply your changes.  
```sh
cargo install --path .
```
## Output
After you run the command, it will output the generated file into `invoice.pdf`:
```sh
open invoice.pdf
```

<img width="600" alt="Invoice" src="https://ak-portfolio-assets.s3.eu-west-2.amazonaws.com/Screenshot+from+2023-11-14+17-30-51.png">

## Contact
For any queries, feel free to reach me directly at anton@quorum.ltd.