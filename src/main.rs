use chrono::Local;
use clap::Parser;
use printpdf::Mm;

mod config;
mod pdf;
mod helpers {
    pub(crate) mod combine_vectors;
    pub(crate) mod generate_id;
    pub(crate) mod hyphenate;
}

use crate::config::*;
use crate::helpers::generate_id::*;
use crate::helpers::hyphenate::*;
use crate::pdf::*;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(short, long, default_value = FROM)]
    from: String,

    #[arg(short, long, default_value = TO)]
    to: String,

    #[arg(short, long, use_value_delimiter = true, value_delimiter = ',', default_value = ITEM)]
    item: Vec<String>,

    #[arg(
        short,
        long = "qty",
        use_value_delimiter = true,
        value_delimiter = ',',
        default_value = QTY, 
    )]
    quantity: Vec<i32>,

    #[arg(
        short,
        long,
        default_value = RATE,
        use_value_delimiter = true,
        value_delimiter = ','
    )]
    rate: Vec<f32>,

    #[arg(short = 'x', long, default_value_t = TAX)]
    tax: f32,

    #[arg(short = '%', long, default_value_t = DISCOUNT)]
    discount: f32,

    #[arg(short, long, default_value = NOTE)]
    note: String,

    #[arg(short, long, default_value_t = String::from(CURRENCY))]
    currency: String,

    #[arg(short, long, default_value_t = Local::now().format("%B %d, %Y").to_string())]
    date: String,
}

pub struct Meta {
    id: String,
    title: String,
    page_width: Mm,
    page_height: Mm,
    margin: Mm,
    logo_max_height: Mm,
    logo_max_width: Mm,
    headers: Vec<(String, Mm)>,
}

pub struct Invoice {
    meta: Meta,
    props: Args,
}

fn main() {
    let invoice = Invoice {
        meta: Meta {
            id: generate_id(8).hyphenate(),
            title: "Invoice".to_string(),
            page_width: Mm(210.0),  // A4 width
            page_height: Mm(297.0), // A4 height
            margin: Mm(20.0),       // 20mm margin
            logo_max_height: Mm(25.0),
            logo_max_width: Mm(100.0),
            headers: vec![
                ("ITEM".to_string(), Mm(90.0)),
                ("RATE".to_string(), Mm(35.0)),
                ("QTY".to_string(), Mm(20.0)),
                ("AMOUNT".to_string(), Mm(30.0)),
            ],
        },
        props: Args::parse(),
    };
    generate_invoice(invoice);
}
