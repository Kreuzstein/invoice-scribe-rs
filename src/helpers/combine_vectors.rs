pub fn combine_vectors(vec_tuple: (&Vec<String>, &Vec<i32>, &Vec<f32>)) -> Vec<(String, i32, f32)> {
    let (vec1, vec2, vec3) = vec_tuple;

    // Use iterators and zip to combine the three vectors into a single vector of tuples.
    let combined_vec: Vec<(String, i32, f32)> = vec1.iter()
        .cloned()    // Clone String to give ownership to the tuple
        .zip(vec2)   // Pair elements from the first and second vectors
        .zip(vec3)   // Pair previously paired elements with elements from the third vector
        .map(|((s, i), f)| (s, *i, *f)) // Adjust the types within the tuple
        .collect();  // Collect into a vector of tuples

    combined_vec
}
