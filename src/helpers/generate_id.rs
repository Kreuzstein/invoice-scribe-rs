use rand::{thread_rng, Rng};

pub fn generate_id(length: usize) -> String {
    // Define the characters you want to include (excluding lookalike characters)
    let chars = b"ACEFHJKLMNPQRTUVWXY3479";
    let mut rng = thread_rng();

    // Sample 'length' times from the provided characters to build the string
    let id: String = (0..length)
        .map(|_| {
            let idx = rng.gen_range(0..chars.len());
            chars[idx] as char
        })
        .collect();

    id
}
