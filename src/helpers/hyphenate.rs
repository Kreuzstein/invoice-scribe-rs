pub trait Hyphenate {
    fn hyphenate(&self) -> String;
}

impl Hyphenate for String {
    fn hyphenate(&self) -> String {
        self.as_bytes() // Use as_bytes to iterate efficiently over ASCII characters
            .chunks(4) // Create chunks of 4 bytes (characters)
            .map(|chunk| std::str::from_utf8(chunk).unwrap()) // Convert each chunk to a string slice
            .collect::<Vec<&str>>()
            .join("-") // Join the string slices with a hyphen
    }
}
