use std::env;
use std::fs::File;
use std::io::BufWriter;
use std::path::Path;

use ::image::io::Reader as ImageReader;
use printpdf::*;

use crate::config::FONT_PATH;
use crate::helpers::combine_vectors::combine_vectors;
use crate::*;

type Cursor = (Mm, Mm);

fn inches_to_millimeters(inches: f32) -> Mm {
    Mm(inches * 25.4)
}

fn calculate_scale_coefficients(image_size: (Mm, Mm), max_width: Mm, max_height: Mm) -> (f32, f32) {
    let (image_width, image_height) = image_size;

    let scale_x = max_width.0 / image_width.0;
    let scale_y = max_height.0 / image_height.0;

    let scale_factor = scale_x.max(scale_y).min(1.0); // Use the smaller of the two, but not more than 1.0

    (scale_factor, scale_factor) // Use uniform scaling to maintain aspect ratio
}

fn add_logo(layer: &PdfLayerReference, meta: &Meta, cursor: &mut Cursor) {
    let s = &([
        env::current_dir().unwrap().to_str().unwrap().to_string(),
        LOGO_PATH.to_string(),
    ]
    .join("/"));

    let path = Path::new(s);
    let dynamic_image = match ImageReader::open(path).unwrap().decode() {
        Ok(image) => image,
        Err(_) => {
            return;
        }
    };
    let image_rgb = dynamic_image.to_rgb8();

    // Get the image dimensions
    let (width_px, height_px) = image_rgb.dimensions();
    let dpi = 300.0;

    // Convert image data to a format that printpdf can use
    let image = Image::from(ImageXObject {
        width: Px(width_px as usize),
        height: Px(height_px as usize),
        color_space: ColorSpace::Rgb,
        bits_per_component: ColorBits::Bit8,
        interpolate: true,
        image_data: image_rgb.into_raw(),
        image_filter: None,
        clipping_bbox: None,
    });

    let (scale_x, scale_y) = calculate_scale_coefficients(
        (meta.page_width, meta.page_height),
        meta.logo_max_width,
        meta.logo_max_height,
    );
    cursor.1 = cursor.1 - inches_to_millimeters(height_px as f32 / dpi) * scale_y;
    // Insert the image into the PDF
    image.add_to_layer(
        layer.clone(),
        ImageTransform {
            translate_x: Some(cursor.0),
            translate_y: Some(cursor.1),
            rotate: None,
            scale_x: Some(scale_x),
            scale_y: Some(scale_y),
            dpi: Some(dpi),
        },
    );
}

fn add_text(
    layer: &PdfLayerReference,
    cursor: &mut Cursor,
    text: &String,
    font_size: f32,
    font: &IndirectFontRef,
) {
    cursor.1 = cursor.1 - Mm(font_size / 2.0);
    layer.use_text(text, font_size, cursor.0, cursor.1, &font);
}

fn add_line_break(cursor: &mut Cursor, line_height: Mm) {
    cursor.1 = cursor.1 - line_height;
}

fn add_divider(layer: &PdfLayerReference, cursor: &mut Cursor, meta: &Meta) {
    add_line_break(cursor, Mm(3.0));
    let start_point = Point::new(cursor.0, cursor.1);
    let end_point = Point::new(cursor.0 + meta.page_width - (meta.margin * 2.0), cursor.1);

    let line = Line {
        points: vec![(start_point, false), (end_point, false)], // false means the line is not curved
        is_closed: false,
    };
    layer.add_line(line);
    add_line_break(cursor, Mm(3.0));
}

fn add_items_table_headers(
    layer: &PdfLayerReference,
    cursor: &mut Cursor,
    meta: &Meta,
    font: &IndirectFontRef,
) {
    add_line_break(cursor, Mm(12.0));
    for (label, offset) in &meta.headers {
        layer.use_text(label, 10.0, cursor.0, cursor.1, &font);
        cursor.0 = cursor.0 + offset.to_owned();
    }
    cursor.0 = meta.margin;
}

fn add_items_table_rows(
    layer: &PdfLayerReference,
    cursor: &mut Cursor,
    meta: &Meta,
    font: &IndirectFontRef,
    args: (&Vec<String>, &Vec<i32>, &Vec<f32>),
    currency: &str,
) {
    let rows = combine_vectors(args);
    add_line_break(cursor, Mm(6.0));
    for (item, qty, rate) in rows {
        add_line_break(cursor, Mm(6.0));
        let amount = rate * qty as f32;
        layer.use_text(&item, 12.0, cursor.0, cursor.1, &font);
        cursor.0 = cursor.0 + meta.headers[0].1.to_owned();
        layer.use_text(
            [currency, &format!("{:.2}", &rate)].join(" "),
            12.0,
            cursor.0,
            cursor.1,
            &font,
        );
        cursor.0 = cursor.0 + meta.headers[1].1.to_owned();
        layer.use_text(
            ["x", &qty.to_string()].join(" "),
            12.0,
            cursor.0,
            cursor.1,
            &font,
        );
        cursor.0 = cursor.0 + meta.headers[2].1.to_owned();
        layer.use_text(
            [currency, &format!("{:.2}", &amount)].join(" "),
            12.0,
            cursor.0,
            cursor.1,
            &font,
        );
        cursor.0 = meta.margin;
    }
}

fn add_total(
    layer: &PdfLayerReference,
    cursor: &mut Cursor,
    invoice: &Invoice,
    font: &IndirectFontRef,
) {
    let min_y = invoice.meta.margin + Mm(50.0);
    if cursor.1 > min_y {
        cursor.1 = min_y;
    }
    add_divider(layer, cursor, &invoice.meta);
    add_text(layer, cursor, &"Note".to_string(), 10.0, font);
    add_text(layer, cursor, &invoice.props.note, 12.0, font);
    add_line_break(cursor, Mm(-1. * (10. + 12.) / 2.));
    cursor.0 =
        cursor.0 + invoice.meta.headers[0].1.to_owned() + invoice.meta.headers[1].1.to_owned();

    let mut subtotal = 0.;
    let billed_items = combine_vectors((
        &invoice.props.item,
        &invoice.props.quantity,
        &invoice.props.rate,
    ));
    for (_item, qty, rate) in billed_items {
        let amount = rate * qty as f32;
        subtotal += amount;
    }
    add_text(layer, cursor, &"Subtotal".to_string(), 10.0, font);
    add_line_break(cursor, Mm(-1. * 10. / 2.));
    cursor.0 = cursor.0 + invoice.meta.headers[2].1.to_owned();
    add_text(
        layer,
        cursor,
        &[
            invoice.props.currency.to_owned(),
            format!("{:.2}", &subtotal),
        ]
        .join(" "),
        12.0,
        font,
    );
    cursor.0 = cursor.0 - invoice.meta.headers[2].1.to_owned();
    add_line_break(cursor, Mm(3.));

    let mut tax = 0.;
    if invoice.props.tax > 0. {
        add_text(layer, cursor, &"Tax".to_string(), 10.0, font);
        tax = &subtotal * (invoice.props.tax / 100.);
        add_line_break(cursor, Mm(-1. * 10. / 2.));
        cursor.0 = cursor.0 + invoice.meta.headers[2].1.to_owned();
        add_text(
            layer,
            cursor,
            &[invoice.props.currency.to_owned(), format!("{:.2}", &tax)].join(" "),
            12.0,
            font,
        );
        cursor.0 = cursor.0 - invoice.meta.headers[2].1.to_owned();
        add_line_break(cursor, Mm(3.));
    };

    let mut discount = 0.;
    if invoice.props.discount > 0. {
        add_text(layer, cursor, &"Discount".to_string(), 10.0, font);
        discount = (&subtotal + &tax) * (invoice.props.discount / 100.);
        add_line_break(cursor, Mm(-1. * 10. / 2.));
        cursor.0 = cursor.0 + invoice.meta.headers[2].1.to_owned();
        add_text(
            layer,
            cursor,
            &[
                "—".to_string(),
                invoice.props.currency.to_owned(),
                format!("{:.2}", &discount),
            ]
            .join(" "),
            12.0,
            font,
        );
        cursor.0 = cursor.0 - invoice.meta.headers[2].1.to_owned();
        add_line_break(cursor, Mm(3.));
    };

    let total = subtotal + tax - discount;
    add_text(layer, cursor, &"Total".to_string(), 10.0, font);
    add_line_break(cursor, Mm(-1. * 10. / 2.));
    cursor.0 = cursor.0 + invoice.meta.headers[2].1.to_owned();
    add_text(
        layer,
        cursor,
        &[invoice.props.currency.to_owned(), format!("{:.2}", &total)].join(" "),
        12.0,
        font,
    );
    add_line_break(cursor, Mm(3.));
    cursor.0 = invoice.meta.margin;
}

pub fn generate_invoice(invoice: Invoice) {
    let title = [&invoice.meta.title, " #", &invoice.meta.id].join("");

    // Position the cursor on top of the page, respecting the specified margins
    let mut cursor: Cursor = (
        invoice.meta.margin.clone(),
        (invoice.meta.page_height - invoice.meta.margin),
    );

    // Create a new PDF with one page
    let (doc, page1, layer1) = PdfDocument::new(
        &title,
        invoice.meta.page_width,
        invoice.meta.page_height,
        "Layer 1",
    );
    let current_layer = doc.get_page(page1).get_layer(layer1);

    // Load a font
    let font = doc
        .add_external_font(File::open(FONT_PATH).unwrap())
        .unwrap();

    add_logo(&current_layer, &invoice.meta, &mut cursor);
    add_text(&current_layer, &mut cursor, &title, 24.0, &font);
    add_line_break(&mut cursor, Mm(6.0));
    add_text(
        &current_layer,
        &mut cursor,
        &"From:".to_string(),
        12.0,
        &font,
    );
    add_text(
        &current_layer,
        &mut cursor,
        &invoice.props.from,
        12.0,
        &font,
    );
    add_divider(&current_layer, &mut cursor, &invoice.meta);
    add_text(
        &current_layer,
        &mut cursor,
        &"Invoice date:".to_string(),
        8.0,
        &font,
    );
    add_text(
        &current_layer,
        &mut cursor,
        &invoice.props.date,
        12.0,
        &font,
    );
    add_line_break(&mut cursor, Mm(3.0));
    add_text(&current_layer, &mut cursor, &"To:".to_string(), 8.0, &font);
    add_text(&current_layer, &mut cursor, &invoice.props.to, 12.0, &font);
    add_divider(&current_layer, &mut cursor, &invoice.meta);
    add_items_table_headers(&current_layer, &mut cursor, &invoice.meta, &font);
    add_items_table_rows(
        &current_layer,
        &mut cursor,
        &invoice.meta,
        &font,
        (
            &invoice.props.item,
            &invoice.props.quantity,
            &invoice.props.rate,
        ),
        &invoice.props.currency,
    );
    add_total(&current_layer, &mut cursor, &invoice, &font);
    // Save the PDF
    doc.save(&mut BufWriter::new(File::create("invoice.pdf").unwrap()))
        .unwrap();
}
