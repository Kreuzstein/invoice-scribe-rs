// Path to assets
pub const LOGO_PATH: &str = "logo.png";
pub const FONT_PATH: &str = "assets/fonts/IBMPlexSerif-Regular.ttf";
// Defaults
pub const FROM: &str = "My Company";
pub const TO: &str = "My Client";
pub const ITEM: &str = "My Service";
pub const QTY: &str = "1"; // Must be an integer
pub const RATE: &str = "0.";
pub const TAX: f32 = 0.; // Input as a percentage, e.g. 20 or 20.0 for 20%
pub const DISCOUNT: f32 = 0.; // Input as a percentage, e.g. 20 or 20.0 for 20%
pub const CURRENCY: &str = "£";
pub const NOTE: &str = "";
